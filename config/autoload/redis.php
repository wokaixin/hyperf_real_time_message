<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    'default' => [
        'host' => env('REDIS_HOST', '192.168.10.1'),
        'auth' => env('REDIS_AUTH', '123456'),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => (int) env('REDIS_DB', 0),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],// 增加一个名为 wsFd 的 Redis 连接池 专用记录用户fd连接信息对应用户uid的信息
    'wsFd' => [
        'host' => env('REDIS_HOST', '192.168.10.1'),
        'auth' => env('REDIS_AUTH', '123456'),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => 1,
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
    // 增加一个名为 wsUid 的 Redis 连接池 专用记录用户id对应用户连接fd的信息
    'wsUid' => [
        'host' => env('REDIS_HOST', '192.168.10.1'),
        'auth' => env('REDIS_AUTH', '123456'),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => 2,
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
    // 增加一个名为 clientIp 的 Redis 连接池 缓存记录用户ip信息，记录一个心跳时间内，用户ip连接次数
    'clientIp' => [
        'host' => env('REDIS_HOST', '192.168.10.1'),
        'auth' => env('REDIS_AUTH', '123456'),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => 3,
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
    // 增加一个名为 abnormalIp 的 Redis 连接池 缓存记录异常用户ip信息，记录一个心跳时间内，用户ip连接次数超出次数，会被记录，缓存时间7天
    'abnormalIp' => [
        'host' => env('REDIS_HOST', '192.168.10.1'),
        'auth' => env('REDIS_AUTH', '123456'),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => 4,
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
];
