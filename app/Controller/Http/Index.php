<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Http;
use Hyperf\Server\ServerFactory;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use App\JsonRpc\CalculatorServiceInterface;
class Index extends  \App\Controller\Http\Base
{

// 根据token获取用户id
    public function getUid(){
        $input = $this->request->all();
        if (isset($input['token'])) {
            if ($input['token']==='123456') {
                // 返回用户 uid 
                $input['uid']='123456';
                return ['result'=>$input,'code'=>'0'];
            }
            # code...
        }
        return ['fail'=>$input,'code'=>'1'];
    }
    public function index()
    {

        $input = $this->request->all();
        $input['uid']='123456';
        $input['nickname']='小龙';
        $input['headimg']='http://img.jpg';
        return ['result'=>$input,'code'=>'0'];
    }
}
?>