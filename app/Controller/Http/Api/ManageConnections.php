<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Http\Api;
use Hyperf\Server\ServerFactory;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use App\Model\User;
class ManageConnections extends \App\Controller\Http\Base
{
    // 根据连接fd 强制断开连接，需先根据用户id 到getUidFd获取到fd信息
    //  {"data":{"fds":["1"]},"token":"令牌"}
    public function closeFds(){
        $params = $this->request->all();
        $data=$params['data'];
        $count=0;
        if (isset($data['fds'])) {
            $fds=$data['fds'];
            $container= ApplicationContext::getContainer();
            $server=$container->get(ServerFactory::class)->getServer()->getServer();
            foreach ($fds as $fd) {
                $fd=intval($fd);
                // 需要先判断是否是正确的websocket连接，否则有可能会push失败
                if ($server->isEstablished($fd)) {
                    $count++;
                    $server->disconnect($fd,401, "连接已断开");
                    // $onlineFdList[]=$fd;
                }
            }
            return ['code'=>0,'result'=>['count'=>$count],'message'=>'ok'];
        }else{
            return ['code'=>1,'fail'=>'数据格式有误'];
        }
    }
    // 强制断开全部连接
    //  {"data":{},"token":"令牌"}
    public function closeAll(){
            $params = $this->request->all();
            $count=0;
            $container= ApplicationContext::getContainer();
            $server=$container->get(ServerFactory::class)->getServer()->getServer();
            $fdList=$server->connections;
            foreach ($fdList as $fd) {
                $fd=intval($fd);
                if ($server->isEstablished($fd)) {
                    $count++;
                    $server->disconnect($fd,401, "连接已断开");
                }
             }
        return ['code'=>0,'result'=>['count'=>$count],'message'=>'ok'];
    }
    // 根据uid 断开连接，支持多uid
    //  {"data":{"uids":["123456"]},"token":"令牌"}
    public function closeUids(){
        $params = $this->request->all();
        $data=$params['data'];
        $count=0;
        if (isset($data['uids']) && is_array($data['uids'])) {
            $uids=$data['uids'];
            $container= ApplicationContext::getContainer();

            $redis=$container->get(RedisFactory::class)->get('wsUid');
            $server=$container->get(ServerFactory::class)->getServer()->getServer();
             foreach ($uids as $k => $uid) {
                if (is_int($uid)) {
                   $uid=''.$uid;
                }
                $fdList=$redis->smembers($uid)??[];

                 foreach ($fdList as $key => $fd) {
                    $fd=intval($fd);
                    if ($server->isEstablished($fd)) {
                        $count++;
                        $server->disconnect($fd,401, "连接已断开");
                    }else{
                        //移除集合中指定的fd
                       $redis->sRem($uid, $fd);
                    }
                 }
                
             }
           }else{
                return ['code'=>1,'fail'=>'数据格式有误'];
           }
        return ['code'=>0,'result'=>['count'=>$count],'message'=>'ok'];
    }
    // 根据用户id获取连接fd
    // {"data":{"uids":["123456"]},"token":"令牌"}
    public function uidFds(){
        $params = $this->request->all();
        $data=$params['data'];
        if (isset($data['uids'])) {
            $container= ApplicationContext::getContainer();
            $list=[];
            $redis = $container->get(RedisFactory::class)->get('wsUid');
                $uids=$data['uids'];
                if(is_array($uids)){
                    foreach ($uids as $k => $uid){
                        if (is_int($uid)) {
                           $uid=''.$uid;
                        }
                        $list[]=$redis->smembers($uid)??[];
                    }
                }
            return ['code'=>0,'result'=>['fds'=>$list],'message'=>'ok'];
        }else{
            return ['code'=>1,'fail'=>'数据格式有误'];
        }
    }
        // 获取所有在线用户
    // {"data":{}}
    public function uids(){
        $params = $this->request->all();
        $data=$params['data'];
        $container= ApplicationContext::getContainer();
        $redis = $container->get(RedisFactory::class)->get('wsUid');
        $list=$redis->keys("*")??[];
        return ['code'=>0,'result'=>['list'=>$list],'message'=>'ok'];

    }
}