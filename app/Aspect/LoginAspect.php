<?php

namespace App\Aspect;

use App\Controller\Http\Push;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
/**
 * @Aspect()
 */
class LoginAspect extends AbstractAspect
{

    public $classes = [
        // Push::class . '::' . 'all',
    ];

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $result = $proceedingJoinPoint->process();
        if (isset($result['token']) && $result['token']==='123456') {
            return $result;
        }else{
            return ['fail'=>'未授权','code'=>401];
        }
    }
}